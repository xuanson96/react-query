
# Sử dụng một base image có chứa Node.js
FROM node:18.18.2 as build

# Thiết lập thư mục làm việc
WORKDIR /app

# Sao chép package.json và package-lock.json vào thư mục làm việc
COPY package*.json ./

# Cài đặt các dependencies
RUN yarn

# Sao chép mã nguồn của ứng dụng vào thư mục làm việc
COPY . .

# Xây dựng ứng dụng Next.js
RUN yarn build

FROM nginx:stable-alpine

RUN apk add --no-cache npm && npm install pm2 -g

# COPY --from=build --chown=nginx:nginx /app/.next/standalone /usr/share/nginx/html/.next/standalone
# COPY --from=build --chown=nginx:nginx /app/.next/static /usr/share/nginx/html/.next/static

COPY nginx/default.conf /etc/nginx/conf.d/

COPY --from=build /app/.next /app/.next
COPY --from=build /app/public /app/public
COPY --from=build /app/package*.json /app/

# Install app dependencies
RUN cd /app && npm install --production

# Expose cổng 3000 để kết nối với ứng dụng
EXPOSE 3000


# EXPOSE 80
# Start Nginx server
# CMD ["nginx", "-g", "daemon off;"]
CMD nginx && pm2-runtime start npm -- -w /app --name my-next-app -- start